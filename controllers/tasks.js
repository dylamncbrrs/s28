//[SECTION] Dependencies and Modules
const Task = require('../models/Task');

//[SECTION] Functionalities

	//Create New Task
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name
		let newTask = new Task({
			name: taskName
		}); 
		return newTask.save().then((task, error) => {
			if (error) {
				return 'Saving New Task Failed';
			} else {
				return 'A New Task Created!';
			}
		})
	}

	//Retrieve All Tasks
	module.exports.getAllTasks = () => {
		return Task.find({}).then(searchResult => {
			return searchResult; 
		})
	}
